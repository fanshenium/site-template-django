from django.contrib.auth.models import User
from rest_framework import serializers, viewsets


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(required=True, max_length=255)
    email = serializers.CharField(required=False, allow_blank=True, max_length=255)
    is_staff = serializers.BooleanField(required=False)
